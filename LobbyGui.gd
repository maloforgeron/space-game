extends PanelContainer


# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	multiplayer.connect("connected_to_server", Callable(self, "connected"))
	multiplayer.connect("server_disconnected", Callable(self, "disconnected"))

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta: float) -> void:
	pass

func connected():
	visible = true

func disconnected():
	visible = false
