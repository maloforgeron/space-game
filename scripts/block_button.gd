extends Button

var block_name = ""
# Called when the node enters the scene tree for the first time.
func _ready():
	var block = load("res://rock_to_block/" + block_name).instantiate()
	$MarginContainer/VBoxContainer/TextureRect.texture = block.get_node("preview").texture
	$MarginContainer/VBoxContainer/Label.text = block.name


func _on_pressed():
	get_node("/root/build_mode").update_block(block_name)
