extends Object

var server := UDPServer.new()
var peers = []

var mpPeer = ENetMultiplayerPeer.new()

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	server.listen(6969)
	var err = mpPeer.create_server(1234)
	print("server: " + str(err))


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta: float) -> void:
	server.poll()
	
	if server.is_connection_available():
		var peer : PacketPeerUDP = server.take_connection()
		var packet = peer.get_packet()
		print("Accepted peer: %s:%s" % [peer.get_packet_ip(), peer.get_packet_port()])
		print("Received data: %s" % [packet.get_string_from_utf8()])
		# Reply so it knows we received the message.
		peer.put_packet("ok".to_utf8_buffer())
		# Keep a reference so we can keep contacting the remote peer.
		peers.append(peer)

