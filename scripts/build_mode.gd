extends Control

var build_mode = true
var current_block
var current_block_type
var grid = []
var grid_width = 256
var grid_height = 256
var holding_block = false
var eraser_mode = false
var current_rotation = 0

# Called when the node enters the scene tree for the first time.
func _ready():
	for x in grid_width:
		grid.append([])
		for y in grid_height:
			grid[x].append(null) # Set a starter value for each position

	var dir = DirAccess.open("res://rock_to_block/")
	if dir:
		dir.list_dir_begin()
		var file_name = dir.get_next()
		var button_resource = load("res://block_button.tscn")
		while file_name != "":
			if dir.current_is_dir():
				print("Found directory: " + file_name)
			else:
				var button = button_resource.instantiate()
				button.block_name = file_name
				$PanelContainer/ScrollContainer/MarginContainer/GridContainer.add_child(button)
				file_name = dir.get_next()


func update_block(block_name):
	current_block_type = load("res://rock_to_block/" + block_name)
	var instantiated_block = current_block_type.instantiate()
	get_tree().root.add_child(instantiated_block)
	if current_block != null:
		current_block.queue_free()
	current_block = instantiated_block
	holding_block = false
	eraser_mode = false


func clear_block():
	if current_block != null:
		current_block.queue_free()
		holding_block = false
	else:
		eraser_mode = true


func place_block(pos):
	var pos_dif = current_block.position - pos
	var steps = 10
	for i in steps:
		var point: float = i
		var point_dif = pos_dif * ((point+1)/steps)
		var point_pos = ((pos + point_dif) / 32).round()
		if grid[point_pos.x][point_pos.y] == null:
			current_block.get_node("preview").modulate = Color(1, 1, 1)
			current_block.position = point_pos * 32
			var new_block = current_block_type.instantiate()
			new_block.position = point_pos * 32
			new_block.rotation = current_rotation 
			get_tree().root.add_child(new_block)
			grid[point_pos.x][point_pos.y] = current_block
			current_block = new_block


func delete_block(pos):
	if grid[pos.x][pos.y] != null:
		current_block = grid[pos.x][pos.y]
		grid[pos.x][pos.y] = null
		current_block.queue_free()


func pick_up_block(pos):
	if grid[pos.x][pos.y] != null:
		current_block = grid[pos.x][pos.y]
		current_rotation = current_block.rotation
		grid[pos.x][pos.y] = null
		holding_block = true


func drop_block(pos):
	if grid[pos.x][pos.y] == null:
		current_block.get_node("preview").modulate = Color(1, 1, 1)
		grid[pos.x][pos.y] = current_block
		current_block = null
		holding_block = false


func set_current_block_color(mouse_pos):
	if grid[mouse_pos.x/32][mouse_pos.y/32] != null:
		current_block.get_node("preview").modulate = Color(2, 0.75, 0.75)
	else:
		current_block.get_node("preview").modulate = Color(1.3, 1.3, 1.3)


func rotate_block(direction):
	if direction == "clockwise":
		current_rotation = current_rotation + PI/2
	elif direction == "counterclockwise":
		current_rotation = current_rotation - PI/2
	if current_rotation >= 2*PI or current_rotation <= -2*PI:
		current_rotation = 0
	current_block.rotation = current_rotation


func copy_block(pos):
	if grid[pos.x][pos.y] != null:
		var new_block = grid[pos.x][pos.y].duplicate(8)
		new_block.position = pos * 32
		new_block.rotation = current_rotation 
		get_tree().root.add_child(new_block)
		current_block = new_block
	elif current_block != null:
		current_block.queue_free()
		current_block = null


func _process(delta):
	pass


func _input(event):
	if build_mode == true:
		if event.is_action_released("E") and current_block != null:
			rotate_block("clockwise")
		elif event.is_action_released("Q") and current_block != null:
			rotate_block("counterclockwise")
		if event.is_action_pressed("ui_cancel"):
			clear_block()
		if event is InputEventMouse:
			var rounded_mouse_pos = (event.position/32).round() * 32
			if event.is_action_released("RMB"):
				copy_block(rounded_mouse_pos/32)
			if event.is_action_released("LMB") and eraser_mode == false:
				if holding_block == false and current_block == null:
					pick_up_block(rounded_mouse_pos/32)
				elif holding_block == true and current_block != null:
					drop_block(rounded_mouse_pos/32)
			if current_block != null:
				set_current_block_color(rounded_mouse_pos)
				if event.button_mask == 1 and holding_block == false:
					place_block(rounded_mouse_pos)
				current_block.position = rounded_mouse_pos
			else:
				if event.button_mask == 1 and eraser_mode == true:
					delete_block(rounded_mouse_pos/32)

