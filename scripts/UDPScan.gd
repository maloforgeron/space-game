extends Node

var udp := PacketPeerUDP.new()
var udp_connected
var children

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	children = get_children()
	udp.set_dest_address("255.255.255.255", 6969)
	udp.set_broadcast_enabled(true)


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta: float) -> void:
	if !udp_connected:
		# Try to contact the server
		udp.put_packet("space?".to_utf8_buffer())

	if udp.get_available_packet_count() != 0:
		print("Reply received: " + udp.get_packet().get_string_from_utf8())
		
		if find_children(udp.get_packet_ip()) != null:
			# if IP.get_local_addresses().find(udp.get_packet_ip()) == -1:
				var button := Button.new()
				button.connect("pressed", Callable(self, "join_server").bind(udp.get_packet_ip()))
				button.text = udp.get_packet_ip()
				add_child(button)

func join_server(ip):
	var mpPeer = ENetMultiplayerPeer.new()
	var err = mpPeer.create_client(ip, 1234)
	print("client: " + str(err))
	multiplayer.multiplayer_peer = mpPeer
	print(multiplayer.get_peers())

